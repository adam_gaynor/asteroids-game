"use strict";

(function () {
  if (typeof Asteroids === "undefined") {
    window.Asteroids = {};
  }

  var MAX_SPEED = 6;
  var COLOR = "#FF3030";
  var RADIUS = 25;

  var Asteroid = Asteroids.Asteroid = function (options) {
    Asteroids.MovingObject.call(
      this,
      {
        pos: options.pos,
        vel: Asteroids.Util.randomVec(MAX_SPEED),
        color: COLOR,
        radius: RADIUS,
        game: options.game
      }
    )
  }

  Asteroids.Util.inherits(Asteroid, Asteroids.MovingObject);
})();
