"use strict";

(function () {
  if (typeof Asteroids === "undefined") {
    window.Asteroids = {};
  }

  var DIM_X = 700;
  var DIM_Y = 700;
  var NUM_ASTEROIDS = 20;

  var Game = Asteroids.Game = function (options) {
    this.asteroids = [];
    this.addAsteroids();
    this.dimX = DIM_X;
    this.dimY = DIM_Y;
  };

  Game.prototype.addAsteroids = function() {
    for (var i = 0; i < NUM_ASTEROIDS; i++) {
      var position = this.randomPosition();
      var newAsteroid = new Asteroids.Asteroid({
        pos: position,
        game: this
      });
      this.asteroids.push(newAsteroid);
    }
  };

  Game.prototype.randomPosition = function () {
    var positionX = Math.random() * DIM_X;
    var positionY = Math.random() * DIM_Y;

    return [positionX, positionY];
  };

  Game.prototype.draw = function (ctx) {
    ctx.clearRect(0, 0, DIM_X, DIM_Y);

    this.asteroids.forEach(function(object) {
      object.draw(ctx);
    });
  };

  Game.prototype.moveObjects = function () {
    this.asteroids.forEach(function(object) {
      object.move();
    });
  };

  Game.prototype.step = function () {
    this.moveObjects();
    this.checkCollisions();
  };

  Game.prototype.checkCollisions = function () {
    for (var i = 0; i < this.asteroids.length - 1; i++) {
      for (var j = i + 1; j < this.asteroids.length; j++) {
        if (this.asteroids[i].isCollidedWith(this.asteroids[j])) {
          this.asteroids[i].collideWith(this.asteroids[j]);
        }
      }
    }
  };

  Game.prototype.remove = function (asteroid) {
    var i = this.asteroids.indexOf(asteroid);
    this.asteroids.splice(i, 1);
  };

  Game.prototype.wrap = function (pos) {
    var x_dim = pos[0];
    var y_dim = pos[1];
    var z = this.asteroids[0].radius + 10;

    x_dim = (x_dim > DIM_X + z) ? -z : x_dim;
    x_dim = (x_dim < -z) ? DIM_X + z : x_dim;
    y_dim = (y_dim > DIM_Y + z) ? -z : y_dim;
    y_dim = (y_dim < -z) ? DIM_Y + z : y_dim;

    return [x_dim, y_dim];
  };

})();
